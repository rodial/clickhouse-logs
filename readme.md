# Логирование посещаемости сайта в ClickHouse

### 1. Схемы таблиц и SQL-запросы

Создание

```sql
CREATE DATABASE site;
```

```sql
CREATE TABLE IF NOT EXISTS site.clicks (
    event_date  DateTime,
    path        String,
    ip          String,
    country	    FixedString(2),
    city        String,
    user_uuid   String,
    referer     String
) ENGINE = MergeTree()
PARTITION BY toYYYYMM(event_date)
ORDER BY (event_date, path, user_uuid)
```
Вставка событий

```sql
INSERT INTO site.clicks (event_date, path, ip, country, city, user_uuid, referer) FORMAT TabSeparated
1540360129	/	20.20.20.20	RU	MOSCOW	u12321434	yandex.ru
1540360130	/about	20.20.20.20	RU	MOSCOW	u12321434	
```
... рабочий запрос если MarkDown не отображает символы табуляции

```sql
INSERT INTO site.clicks (event_date, path, ip, country, city, user_uuid, referer) Values
(1540360131, '/', '20.20.20.20', 'RU', 'MOSCOW', 'u12321434', 'yandex.ru'),
(1540360132, '/about', '20.20.20.20', 'RU', 'MOSCOW', 'u12321434', '')	
```

### 2. Механизм сохранения в ClickHouse логов посещаемости сайта

Вставлять данные в ClickHouse лучше всего пачками. Для этого собираем данные о посещаемости во временном хранилище и отправляем в ClickHouse каждые 5минут|час|сутки. В роли временного хранилища могут выступать файлы, ваша БД ...

Наименее простым способом являются файлы CSV или TSV, т.к. ClickHouse поддердку добавления записей в этих ворматах.

Добавление данных будет наиболее быстрым если записи отсортированы по дате.

### 3. Запрос на получение статистики по посещаемости сайта по дням за последний месяц в разрезе стран

По уникальным пользователям, за текуций месяц

```sql
SELECT
    toDate(event_date) AS ev_date,
    country,
    uniq(user_uuid)
FROM
    site.clicks
WHERE
    ev_date > toStartOfMonth(now())
GROUP BY
    ev_date, country
ORDER BY
    ev_date, country
```

... за месяц (30 дней)

```sql
SELECT
    toDate(event_date) AS ev_date,
    country,
    uniq(user_uuid)
FROM
    site.clicks
WHERE
    ev_date > toDate(subtractMonths(now(), 1))
GROUP BY
    ev_date, country
ORDER BY
    ev_date, country
```

### 4. Запрос на удаление неактуальных данных, более одного года, по пользователям

Не все движки таблиц позволяют удалять данные, для тех которые это не позволяют можно использовать удаление партиций.

Запрос списка партиций

```sql
SELECT * FROM system.parts WHERE database = 'site' AND table = 'clicks'
```

Удаление партиции

```sql
ALTER TABLE site.clicks DROP PARTITION 201809
```

Или можно периодически запускать запрос на удаление партиций старше года

```sql
ALTER TABLE site.clicks DROP PARTITION toYYYYMM(subtractYears(subtractMonths(now(), 1), 1))
```
